// import testing module
const request = require('supertest');


// import app
const app = require('../app');

// User CRUD
describe('TID-UC: Database Interface - Create User', function(){
	it('can make new user', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-UR: Database Interface - Read User', function(){
	it('User Read', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-UU: Database Interface - Update User', function(){
	it('User Update', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-UD: Database Interface - Delete User', function(){
	it('User Delete', function(done){
	    throw new Error('not implemented');
	})
});

// Show CRUD
describe('TID-SC: Database Interface - Create Show', function(){
	it('Show Create', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-SR: Database Interface - Read Show', function(){
	it('Show Read', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-SU: Database Interface - Update Show', function(){
	it('Show Update', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-SD: Database Interface - Delete Show', function(){
	it('Show Delete', function(done){
	    throw new Error('not implemented');
	})
});

// Roster CRUD
describe('TID-RC: Database Interface - Create Roster', function(){
	it('Roster Create', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-RR: Database Interface - Read Roster', function(){
	it('Roster Read', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-RU: Database Interface - Update Roster', function(){
	it('Roster Update', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-RD: Database Interface - Delete Roster', function(){
	it('Roster Delete', function(done){
	    throw new Error('not implemented');
	})
});

// League CRUD
describe('TID-LC: Database Interface - Create League', function(){
	it('League Create', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-LR: Database Interface - Read League', function(){
	it('League Read', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-LU: Database Interface - Update League', function(){
	it('League Update', function(done){
	    throw new Error('not implemented');
	})
});

describe('TID-LD: Database Interface - Delete League', function(){
	it('League Delete', function(done){
	    throw new Error('not implemented');
	})
});
