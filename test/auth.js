// import testing modules
const request = require('supertest');
const expect = require('expect.js');

// import app
const app = require('../app');

// import googleapis module dependency
const {google} = require('googleapis');
var OAuth2 = google.auth.OAuth2;

describe('TIA-U: Auth Interface - OAuth Login URL', function() {
    it('gets login url', function(done) {
	const client = new OAuth2(process.env.GOOGLE_OAUTH_CLIENT_ID,
			      process.env.GOOGLE_OAUTH_CLIENT_SECRET,
			      process.env.GOOGLE_OAUTH_REDIRECTION_URL);

	var scopes = [
	    'https://www.googleapis.com/auth/plus.me',
	    'https://www.googleapis.com/auth/userinfo.email',
	    'https://www.googleapis.com/auth/userinfo.profile'
	];
	
	// generate url
	var url = client.generateAuthUrl({
        access_type: 'offline',
            scope: scopes // If you only need one scope you can pass it as string
	});

	expect(url);
	
	done();
    });
});

describe('TIA-C: Auth Interface - OAuth Callback', function() {
    it('handles callback', function(done) {
	request(app)
	    .get('/auth/oauthCallback')
	    .expect(200, done);
    });
});

describe('TIA-I: Auth Interface - Identify User', function() {
    it('can find user', function(done) {
	throw new Error('not implemented');
    });
});
