// import testing module
const request = require('supertest');

// import app
const app = require('../app');

// Get Home Page
describe('TIR-GH: Routing Interface - Get Home Page', function(){
	it('Get Home Page', function(done){
	    request(app)
			.get('/')
			.expect(200, done);
	})
});

// Get Edit Page
describe('TIR-GE: Routing Interface - Get Edit Page', function(){
	it('Get Edit Settings Page', function(done){
	    request(app)
			.get('/edit')
			.expect(200, done);
	})
});

// Get User data
describe('TIR-GU: Routing Interface - Get User Data', function(){
	it('Get User Data', function(done){
	    request(app)
			.get('/users')
			.expect(200, done);
	})
});

// Get User data
describe('TIR-GS: Routing Interface - Get Show Data', function(){
	it('Get Show Data', function(done){
	    request(app)
			.get('/show')
			.expect(200, done);
	})
});

// Get Roster data
describe('TIR-GR: Routing Interface - Get Roster Data', function(){
	it('Get Roster Data', function(done){
	    request(app)
			.get('/roster')
			.expect(200, done);
	})
});

// Get League data
describe('TIR-GL: Routing Interface - Get League Data', function(){
	it('Get League Data', function(done){
	    request(app)
			.get('/league')
			.expect(200, done);
	})
});
