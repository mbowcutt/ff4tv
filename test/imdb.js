// import testing modules
const request = require('supertest');
const expect = require('expect.js');

// import app
const app = require('../app');

// import imdb package needd for testing
const imdb = require('imdb-api'); 

// show search tests
describe('TII-S: IMDb Interface - Search', function(){

    it('gets expected JSON results for "sunny"', function(done){

	// load expected results
	var json = require('../examples/sunny-search.json')

	// request IMDb
	imdb.search({name: 'Sunny'},
		    {apiKey: process.env.OMDB_API_KEY,timeout: 30000})

	// handle callback
	    .then((search)=>{

		// Assert equal
		expect(search.results).to.eql(json)
	    });		

	// finish
	done();   
    })
});

// show rate tests
describe('TII-R: IMDb Interface - Rate', function(){
	it('gets show scores', function(done){
	    throw new Error('not implemented');
	});
});
