// load node env vars from dotenv if not in production
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').load();
}

// load main dependency modules
var createError = require('http-errors');
var express = require('express');
var Session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


// start app
var app = express();

// attach express-session
app.use(Session({
    secret: 'FF4TV-random-secret-1234567890',
    resave: true,
    saveUninitialized: true
}));

// views engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// misc. express setup
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// load routing modules
var indexRouter = require('./routes/index');
var oauthRouter = require('./routes/oauth');
var userRouter = require('./routes/user');
var showRouter = require('./routes/show');
var rosterRouter = require('./routes/roster');
var leagueRouter = require('./routes/league');

// load routes
app.use('/', indexRouter);
app.use('/auth', oauthRouter);
app.use('/user', userRouter);
app.use('/show', showRouter);
app.use('/roster', rosterRouter);
app.use('/league', leagueRouter);

// handle 404
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
