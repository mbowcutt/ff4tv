// import express router module
var express = require('express');
var router = express.Router();

// import imdb controller module
var imdb = require('./../controllers/imdb');
var show = require('./../models/show');

router.post('/search', async function(req, res) {
    var results = await imdb.search(req.body.title);
    res.render('add', {results: results});
});

router.get('/getrating/:query', async function(req, res){
    var results = await imdb.getrating(req.params["query"]);
    console.log(results)
});

router.get('/getepisode/:query', async function(req, res){
    var results = await imdb.getepisode(req.params["query"]);
    res.send(results[results.length-2])
});

module.exports = router;
