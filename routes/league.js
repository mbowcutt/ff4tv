// import express router module
var express = require('express');
var router = express.Router();

var db_league = require('./../controllers/league');
var db_roster = require('./../controllers/roster');
var db_user = require('./../controllers/user');

var league = require('./../models/league');
var roster = require('./../models/roster');

// display league page
router.get('/', async function(req, res, next) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
    {
	if(req.session["league"]) {
  	    var rosterids = req.session["league"].rosters;
	    var rosters = [];
	    var i = 0;

	    for (var i = 0; i < rosterids.length; i++)
		rosters[i] = await db_roster.read(rosterids[i]);
	    
	}

	res.render('league', {user: req.session["user"],
			      rosters: rosters,
			      league: req.session["league"]});
    }
});

// create new league
router.post('/create', async function(req, res) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
    {
	var leg = new league.League(req.body.league_name,
				    null,
				    [],
				    req.body.max_users);

	var ros = new roster.Roster(leg.leagueid,
				    req.session["user"].userid,
				    "Your Roster",
				    null);
	
	leg.rosters[0] = ros.rosterid;
	req.session["user"].rosterid = ros.rosterid;
	req.session["user"].admin = true;
	db_user.update(req.session["user"]);

	db_league.create(leg);
	db_roster.create(ros);

	req.session["league"] = leg;
	req.session["roster"] = ros;

	res.redirect('/league');
    }
});

// join league
router.post('/join', async function(req, res) {
    if(!req.session["tokens"])
	res.redirect('/');
    else 
    {
	var check = await db_league.read(req.body.leagueid);
	if (check)
	{
	    var len = check.rosters.length;
	    if (check.max_rosters > len)
	    {
		var ros = new roster.Roster(req.body.leagueid,
					    req.session["user"].userid,
					    "Your Roster Name",
					    null);
		db_roster.create(ros);
		check.rosters[len] = ros.rosterid;
		db_league.update(check);

		req.session["user"].rosterid = ros.rosterid;
		req.session["user"].admin = false;
		db_user.update(req.session["user"]);

		
		req.session["roster"] = ros;
		req.session["league"] = check;
	    }
	}
	res.redirect('/league');
    }
});

router.get('/score', async function(req, res) {
    if (req.session["user"].admin)
	league.score(req);
    res.redirect('/league');
});

router.get('/weekend', async function(req, res) {
    if (req.session["user"].admin)
	league.weekend(req);
    req.session["roster"].lineup = req.session["roster"].next_lineup;
    await db_roster.update(req.session["roster"]);
    res.redirect('/league');
});

router.get('/leave', async function(req, res) {
    if (!req.session["user"].admin)
    {
	league.remove(req);
	
        // delete the roster from the db and remove from session
	await db_roster.delete(req.session["roster"].rosterid);
	req.session["roster"] = null;

	// remove rosterid from user and set admin to null
	req.session["user"].rosterid = null;
	req.session["user"].admin = null;
	await db_user.update(req.session["user"]);
	
	// update the league object and remove from session
	await db_league.update(req.session["league"]);
	req.session["league"] = null;
    }
    
    res.redirect('/league');
});

router.get('/delete', async function(req, res) {
    if (req.session["user"].admin)
    {
	await league.delete(req);
	req.session["league"] = null;
	req.session["roster"] = null;

	req.session["user"].rosterid = null;
	req.session["user"].admin = null;

    }
    res.redirect('/league');
});

router.post('/setname', async function(req, res) {
    if (req.session["user"].admin)
    {
	req.session["league"].name = req.body.name;
	await db_league.update(req.session["league"]);
    }
    res.redirect('/settings')
});

module.exports = router;
