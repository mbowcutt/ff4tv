// import express router
var express = require('express');
var router = express.Router();

const { Pool, Client } = require('pg')

// serve home page
router.get('/', function(req, res, next) {
    res.render('index', {tokens: req.session["tokens"] });
});

// serve settings page
router.get('/settings', function(req, res, next) {
    if (!req.session["tokens"])
	res.redirect('/');
    else
	res.render('settings',
		   {user: req.session["user"],
		    roster: req.session["roster"],
		    league: req.session["league"]});
});

module.exports = router;
