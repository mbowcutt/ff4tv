// import express router module
var express = require('express');
var router = express.Router();

var imdb = require('./../controllers/imdb');
var db_show = require('./../controllers/show');
var db_roster = require('./../controllers/roster');
var show = require('./../models/show');

router.get('/', async function(req, res, next) {
    if(!req.session["tokens"])
	res.redirect('/');
    else if (req.session["roster"])
    {
	var inv = req.session["roster"].inventory;
	var active = [], inactive = [];
	for(var i = 0; i < inv.length; i++)
	{
	    if (req.session["roster"].lineup.indexOf(inv[i]) > -1)
		active.push(inv[i]);
	    else
		inactive.push(inv[i]);
	}
	var lineup = [], bench = [];
	for(var i = 0; i < active.length; i++)
	    lineup[i] = await db_show.read(active[i]);
	for(var i = 0; i < inactive.length; i++)
	    bench[i] = await db_show.read(inactive[i]);
    }
    res.render('roster', {roster: req.session["roster"],
			  lineup: lineup,
			  bench: bench});

});

router.get('/add', function(req, res) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
	res.render('add');
});

router.post('/add', async function(req, res){
    if(Array.isArray(req.body.show)){
	for (var sho of req.body.show) {
	    var s = JSON.parse(sho);
	    var sh = await db_show.read(s.imdbid);
	    if (!sh)
	    {
		sh = new show.Show(s.imdbid,
				   s.title,
				   s.poster,
				   null);
		db_show.create(sh);

	    }
	    
	    req.session["roster"].inventory[req.session["roster"].inventory.length] = sh.imdbid;					 
	}
    }
    else {
	var s = JSON.parse(req.body.show);
	var sh = await db_show.read(s.imdbid);
	if (!sh)
	{
	    sh = new show.Show(s.imdbid,
			       s.title,
			       s.poster,
			       null);
	    db_show.create(sh);
	}
	
	req.session["roster"].inventory[req.session["roster"].inventory.length] = sh.imdbid;					 

    }

    db_roster.update(req.session["roster"]);

    res.redirect('/roster');
});

router.get('/set', async function(req, res) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
    {
	var shows = [],
	    active = [],
	    i = 0;
	for (var showid of req.session["roster"].inventory)
	    shows[i++] = await db_show.read(showid);
	i = 0;
	for (var showid of req.session["roster"].next_lineup)
	    active[i++] = await db_show.read(showid);
	res.render('set', {roster: req.session["roster"],
			   inventory: shows,
			   lineup: active});
    }
});

router.post('/set', async function(req, res) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
    {
	if (Array.isArray(req.body.show))
	{
	    var lin = [];
	    for (var i = 0; i < req.body.show.length; i++)
	    {
		var sh = JSON.parse(req.body.show[i]);
		lin[i] = sh.imdbid;
	    }
	    req.session["roster"].next_lineup = lin;
	}
	else
	{
	    var sh = JSON.parse(req.body.show);
	    req.session["roster"].next_lineup = [sh.imdbid];
	}
	db_roster.update(req.session["roster"]);
	res.redirect('/roster/set');
    }
});

router.get('/remove', async function(req, res) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
    {
	var shows = [],
	    i = 0;
	for (var showid of req.session["roster"].inventory)
	    shows[i++] = await db_show.read(showid)
	res.render('remove', {roster: req.session["roster"],
			      inventory: shows});
    }
});

router.post('/remove', async function(req, res){
    if (!req.session["tokens"])
	res.redirect('/')
    else
    {
	if (Array.isArray(req.body.show))
	{
	    var removeids = [];
	    for (var i = 0; i < req.body.show.length; i++)
	    {
		var sh = JSON.parse(req.body.show[i]);
		var shows = req.session["roster"].inventory;
		var lineup = req.session["roster"].lineup;
		var sindex = shows.indexOf(sh.imdbid);
		var lindex = lineup.indexOf(sh.imdbid);
		if (sindex > -1)
		    shows.splice(sindex, 1)
		if (lindex > -1)
		    lineup.splice(lindex, 1)
		req.session["roster"].inventory = shows;
		req.session["roster"].lineup = lineup;
	    }
	}
	else
	{
	    var sh = JSON.parse(req.body.show);
	    var shows = req.session["roster"].inventory;
	    var lineup = req.session["roster"].lineup;
	    var sindex = shows.indexOf(sh.imdbid);
	    var lindex = lineup.indexOf(sh.imdbid);
	    if (sindex > -1)
		shows.splice(sindex, 1)
	    if (lindex > -1)
		lineup.splice(lindex, 1)
	    req.session["roster"].inventory = shows;
	    req.session["roster"].lineup = lineup;
	}
	db_roster.update(req.session["roster"]);
	res.redirect('/roster');
    }
});

router.post('/setname', async function(req, res) {
    req.session["roster"].name = req.body.name;
    await db_roster.update(req.session["roster"]);
    res.redirect('/settings')
});

module.exports = router;
