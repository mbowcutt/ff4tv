// import express router module
var express = require('express');
var router = express.Router();

// display user profile page
router.get('/', function(req, res, next) {
    if(!req.session["tokens"])
	res.redirect('/');
    else
	res.render('user', {tokens: req.session["tokens"],
			    user: req.session["userdata"]});
});

// return user tokens as JSON
router.get('/tokens', function(req, res){
    res.send(req.session["tokens"]);
});

module.exports = router;
