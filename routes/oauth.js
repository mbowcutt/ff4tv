// import express router
var express = require('express');
var router = express.Router();

// import oauth controller module
var auth = require('./../controllers/oauth.js');
var db_user = require('./../controllers/user.js');
var db_roster = require('./../controllers/roster.js');
var db_league = require('./../controllers/league.js');

/* redirect to google login */
router.get('/', function(req, res, next) {
    var client = auth.getOAuthClient();
    res.redirect(auth.getAuthURL(client));
});

/* recieve oauth callback and redirect to home */
router.get("/oauthCallback", async function(req, res) {
    var oauth2Client = auth.getOAuthClient();
    var code = req.query.code;

    // get tokens
    oauth2Client.getToken(code, async function(err, tokens) {
	if(!err) {
            oauth2Client.setCredentials(tokens);
	    // save tokens to client session	    
            req.session["tokens"]=tokens;
	    //res.redirect("/");
        }
	else{
            res.send("Error!");
	}

	while (!req.session["tokens"]) {};
	
	auth.plus.people.get({ userId: 'me', auth: oauth2Client }, async function(err, response) {
	if(!err) // save google data to client session
	{
	    req.session["userdata"] = response.data;
	    // check for user in db and create if nonexistent
	    var u = await db_user.read(req.session["userdata"].id);
	    if (!u)
	    {
		//console.log(response.data);
		var use = auth.createUser(response.data);
		db_user.create(use);
		req.session["user"] = use;
	    }
	    else
		req.session["user"] = u;
	    while (!req.session["user"]) {};
	    req.session["roster"] = await db_roster.read(req.session["user"].rosterid);
	    if (req.session["roster"])
		req.session["league"] = await db_league.read(req.session["roster"].leagueid);
	    
	    res.redirect("/");	
	}
	else
	    res.send(err);
	});
    });
});

// log out the user
router.get("/logout", function (req, res){
    req.session["tokens"] = null;
    console.log(req.session["userdata"])
    req.session["userdata"] = null;
    res.redirect("/");
});

module.exports = router;
