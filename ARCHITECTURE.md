# [bin/](bin/)

This folder contains the Node server's executable code. www.js is the web server's entry point, called from the `npm start` script.

* [www.js](bin/www.js)

# [controllers/](controllers/)

Controllers provide the models with access to the database. The following files need to be developed:

* user.js
* roster.js
* league.js
* show.js

# [examples](examples/)

Here we have a folder with non-functional code, but snippets that may help the developer understand the program.

These snippets should additionally be used for unit testing.

# [models/](models/)

Models are the objects which the user interacts with. The model files need to be developed:

* user.js
* roster.js
* league.js
* show.js

# [public/](public/)

This folder contains static public objects. Because the HTML is rendered dynamically, we only have CSS stylesheets and client-side JavaScript.

* [stylesheets/](stylesheets/]
- [style.css][stylesheets/style.css)
* [scripts/](scripts/])

# [routes/](routes/)

This directory uses the express module. The Node server recieves an HTTP request at an endpoint and trigger a script. From the client's perspective, they can call these scripts by sending a GET or POST request to the desired endpoint.

We should categorize scripts logically into files. Each file can get their endpoints loaded witha common suffix. For example, the default files index.js and users.js have their endpoints prefixed with "/" and "/users/" respectively. 

* [index.js](routes/index.js)
* [users.js](routes/users.js)

# [test/](test/)

This directory uses the supertest module for unit testing. We can craft HTTP requests to our application and validate the responses.

A good way to start developing would be to first write test cases for an app feature and then write code to achieve the conditions.

* [test.js](test/test.js)

# [views/](views/)

This directory controls the client's view. Currently it uses an interesting markup language called Pug, but we'll probably delete these files and return to HTML, using res.send() instead of res.render().

* [error.pug](views/error.pug)
* [index.pug](views/index.pug)
* [layout.pug](views/layout.pug)

# [.gitlab-ci.yml](.gitlab-ci.yml)

This file tells our GitLab CI/CD system information about our environment. Currently, we define a test and deploy script. The test script should run `npm test` and the deploy script should use the dpl Ruby gem to deploy to our Heroku setup.

[Read more on gitlab.com](https://docs.gitlab.com/ce/ci/yaml/README.html)

# [README.md](README.md)

This file contains general information about the project. More specific information, such as the software architecture overview, will be defined in other files.

# [app.js](app.js)

This is the Node server's entry point. It loads our modules and configures them.

# [ARCHITECTURE.md](ARCHITECTURE.md)

This file contains an overview of the software architecture from the developer's view of the git repository.

# [package.json](package.json)

This file defines meta-information about our project, like the name, version, scripts, and production/development dependencies.

[Read more at npmjs.com](https://docs.npmjs.com/files/package.json)