# FF4TV

This is the development repository for a group software engineering project at CWRU.

It's fantasy football for television!

# Installation and Development Scripts

To get started, clone the repository

```
git clone https://gitlab.com/mbowcutt/ff4tv.git
```

Inside the source directory, install the dependencies.

```
npm install
```

## Start

To start the program, run `npm start`.

## Test

To test the program, run `npm test`.

# Dependencies 

* [cookie-parser](https://www.npmjs.com/package/cookie-parser)
* [debug](https://www.npmjs.com/package/debug)
* [express](http://expressjs.com/)
* [http-errors](https://www.npmjs.com/package/http-errors)
* [imdb-api](https://www.npmjs.com/package/imdb-api)
* [morgan](https://www.npmjs.com/package/morgan)
* [object-hash](https://www.npmjs.com/package/object-hash)
* [pg](https://www.npmjs.com/package/pg)
* [pug](https://pugjs.org/) (to be removed)

# Dev Dependencies

* [express](http://expressjs.com/en/api.html)
* [supertest](https://github.com/visionmedia/supertest#API)
* [dotenv](https://www.npmjs.com/package/dotenv)

## Notes

### Additional Info

Please read the [architecture overview](ARCHITECTURE.md) and [contributing tips](CONTRIBUTING.md).

### Adding and Removing Dependencies

To add a production dependency `pkg` and a development dependency `devpkg`, use 

```
npm install pkg
npm install devpkg --save-dev
```

To remove it,

```
npm remove pkg
npm remove devpkg
```

### GitLab CI/CD with Auto DevOps

This project is enabled with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

### GitLab Express Template

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).