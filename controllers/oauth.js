// import googleapis module dependency
const {google} = require('googleapis');
var OAuth2 = google.auth.OAuth2;

var user = require('./../models/user');

// google + module
exports.plus = google.plus('v1');

// creates a new oauth client
exports.getOAuthClient = function() {
    return new OAuth2(process.env.GOOGLE_OAUTH_CLIENT_ID,
		      process.env.GOOGLE_OAUTH_CLIENT_SECRET,
		      process.env.GOOGLE_OAUTH_REDIRECTION_URL)
};

// creates a unique login url
exports.getAuthURL = function(oauth2Client) {
    // define scopes
    var scopes = [
	'https://www.googleapis.com/auth/plus.me',
	'https://www.googleapis.com/auth/userinfo.email',
	'https://www.googleapis.com/auth/userinfo.profile'
    ];

    // generate url
    var url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes // If you only need one scope you can pass it as string
    });
    
    return url;
};

exports.createUser = function(userdata) {
    return new user.User(userdata.id, null, null);
}
