// Connect to Postgres Database
var postgres = require('pg');
var database = new postgres.Client();
database.connect();

// Read Method
exports.read = async function(userid) {
    var data = await database.query('SELECT * FROM "Objects"."User" WHERE userid = ($1)', [userid]);
    return data.rows[0];
};

// Create Method
exports.create = function(user) {
    database.query('INSERT INTO "Objects"."User" (userid, rosterid, admin) VALUES ($1, $2, $3)', [user.userid, user.rosterid, user.admin]);
};

// Update Method
exports.update = function(user) {
    database.query('UPDATE "Objects"."User" SET rosterid = ($1), admin = ($2) WHERE userid = ($3)', [user.rosterid, user.admin, user.userid]);
};

// Delete Method
exports.delete = function(userid) {
    database.query('DELETE FROM "Objects"."User" WHERE userid = ($1)', [userid]);
};
