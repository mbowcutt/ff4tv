// Connect to Postgres Database
var postgres = require('pg');
var database = new postgres.Client();
database.connect();

//Read Method
exports.read = async function(imdbid) {
	var data = await database.query('SELECT * FROM "Objects"."Show" WHERE imdbid = ($1)', [imdbid]);
    return data.rows[0];
};

//Create Method
exports.create = function(show) {
	database.query('INSERT INTO "Objects"."Show" (imdbid, title, poster_url, current_rating) VALUES ($1, $2, $3, $4)', [show.imdbid, show.title, show.poster_url, show.current_rating]);
};

//Update Method
exports.update = function(show) {
	database.query('UPDATE "Objects"."Show" SET title = ($1), poster_url = ($2), current_rating = ($3) WHERE imdbid = ($4)', [show.title, show.poster_url, show.current_rating, show.imdbid]);
};

//Delete Method
exports.delete = function(imdbid) {
	database.query('DELETE FROM "Objects"."Show" WHERE imdbid = ($1)', [imdbid]);
};

