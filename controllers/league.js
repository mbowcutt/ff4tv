// Connect to Postgres Database
var postgres = require('pg');
var database = new postgres.Client();
database.connect();

//Read Method
exports.read = async function(leagueid) {
	var data = await database.query('SELECT * FROM "Objects"."League" WHERE leagueid = ($1)', [leagueid]);
    return data.rows[0];
};

//Create Method
exports.create = function(league) {
	database.query('INSERT INTO "Objects"."League" (leagueid, rosters, max_rosters, name, description) VALUES ($1, $2, $3, $4, $5)', [league.leagueid, league.rosters, league.max_rosters, league.name, league.description]);
};

//Update Method
exports.update = function(league) {
	database.query('UPDATE "Objects"."League" SET rosters = ($1), max_rosters = ($2), name = ($3), description = ($4) WHERE leagueid = ($5)', [league.rosters, league.max_rosters, league.name, league.description, league.leagueid]);
};

//Delete Method
exports.delete = function(leagueid) {
	database.query('DELETE FROM "Objects"."League" WHERE leagueid = ($1)', [leagueid]);
};


