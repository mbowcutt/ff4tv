// Connect to Postgres Database
var postgres = require('pg');
var database = new postgres.Client();
database.connect();

//Read Method
exports.read = async function(rosterid) {
	var data = await database.query('SELECT * FROM "Objects"."Roster" WHERE rosterid = ($1)', [rosterid]);
    return data.rows[0];
};

//Create Method
exports.create = function(roster) {
    database.query('INSERT INTO "Objects"."Roster" (rosterid, userid, leagueid, inventory, lineup, name, description, next_lineup, last_score) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)', [roster.rosterid, roster.userid, roster.leagueid, roster.inventory, roster.lineup, roster.name, roster.description, roster.next_lineup, roster.last_score]);
};

//Update Method
exports.update = function(roster) {
    database.query('UPDATE "Objects"."Roster" SET userid = ($1), leagueid = ($2), inventory = ($3), lineup = ($4), name = ($5), description = ($6), next_lineup = ($7), last_score = ($9) WHERE rosterid = ($8)', [roster.userid, roster.leagueid, roster.inventory, roster.lineup, roster.name, roster.description, roster.next_lineup, roster.rosterid, roster.last_score]);
};

//Delete Method
exports.delete = function(rosterid) {
	database.query('DELETE FROM "Objects"."Roster" WHERE rosterid = ($1)', [rosterid]);
};
