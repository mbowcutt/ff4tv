// import imdb-api module and create client
const imdb = require('imdb-api');
const cli = new imdb.Client({apiKey: process.env.OMDB_API_KEY, timeout: 30000});

// the search function passes a query to IMDb
// search and returns JSON results
exports.search = async function(query) {
    try {
	var results = await cli.search({name: query});
    } catch (err) {
	console.log(err)
    }
    return results;
}

exports.get = async function(query){
	var results = await cli.get({id: query});
	return results;
}


exports.createShow = function(imbdid, title, poster_url, current_rating){
	return new Show(imbdid, title, poster_url, current_rating);
}

exports.getrating = async function(query){
    var results = await cli.get({id: query});
    return parseFloat(results.rating);
}

exports.getepisode = async function(query){
	var results = await cli.get({id: query});
	return results.episodes();
}
