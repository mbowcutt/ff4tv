# Environment Configuration

## The `.env` File

Development for FF4TV requires some node environment variables to be set up for its API config.

In a development environment, create a file called `.env` in the project root directory

```
cd ff4tv
touch .env
```

Inside, you'll need to define the following parameters.

## Google OAUTH

We use Google's OAuth2.0 implementation to identify FF4TV users. Place the following in `.env`:

```
GOOGLE_OAUTH_CLIENT_ID = 537137007450-c9hffqur9rh57qtnbicq60fhm2766kbd.apps.googleusercontent.com
GOOGLE_OAUTH_CLIENT_SECRET = Gn9xDhQfBpySeoVQtHsIQ9Si
GOOGLE_OAUTH_REDIRECTION_URL = http://localhost:3000/auth/oauthCallback

```
### Getting your own keys

NOTE: You can use the example keys above. You do not need to register your own key.

You can create your own OAuth Client ID/Secret pair at the [Google API Console](https://console.developers.google.com/). For more info on that, see [here](https://developers.google.com/identity/protocols/OAuth2).

## OMDB

We use the Open Movie Database to get information on TV shows. The free tier API only allows 1000 requests per day, so each developer need to get their own key.

Request your own key for the OMDb API [here](https://www.omdbapi.com/apikey.aspx). The key will be emailed to you. Replace it with `<APIKEY>` in the following environment variable:

```
OMDB_API_KEY=<APIKEY>
```

## Development DB

The development database for the FF4TV project is a PosgreSQL server managed by Amazon RDS. To connect to the database using the `pg` client, define the following:

```
PGHOST=ff4tv-dev.c8mnsh2dijta.us-east-2.rds.amazonaws.com
PGPORT=5432
PGDATABASE=ff4tv_dev
PGUSER=mbowcutt
PGPASSWORD=12345678
```

# Git CLI Help

## Cloning

Clone the repository when you need to grab fresh source code.

```
git clone git@gitlab.com:mbowcutt/ff4tv.git
```

## Branching

Branching is the good practice of forking the code repository when you work on a new feature. Branch the code when you start working on a task. This creates a bubble of isolation so that you might not step on other coders and cause issues. You'll make some commits for the branch and push them every now and then. When your branch is ready to go into production, make a merge request.

Currently, we have six branches for the purposes briefly described

* master
    - production branch, is directly tied to the [Heroku deployment](https://ff4tv.herokuapp.com/)
* imdb
    - integrate the `imdb-api` node module
    - search for shows
    - score shows
* design
    - create frontend material using `Bootstrap.js`
    - design the Home and Edit page layout
    - design the client interfaces
        - Roster Add/Drop
        - Roster Set Next
        - Roster Info
        - League Join
        - League Info
        - Edit User Settings
        - Edit League Settings
* backend-mvc
    - code object models
    - store and retrieve objects from database
    - provide object info to client
    - process client object change request
* backend-db
    - implement a PostgreSQL database
    - integrate th `pg` node module
* auth
    - integrate Google OAuth
    - redirect client to login if not logged in
    - code user object info

To make a new branch

```
git branch <branchname>
```

To switch to a branch

```
git checkout <branchname>
```

Always make sure you are on the correct branch before committing, pushing, and merging.

## Pulling and Rebasing

Each time you switch to a new branch, you should get the most recent source from master.

```
git rebase <remote>/<branchname>
```

## Committing

When you make a git commit, you're adding functional code to a branch (usually a development branch). Before you make a commit, you must stage the changes made. An easy way to do this is to give git the filename of the files to stage.

```
git add filename
```

Then, make sure you're on a good branch and commit the change with a helpful message.

```
git commit -m "These are my changes and this is what I did"
```

Now they are locked in! You may repeat this very often even for small changes. They add up :)

## Pushing

If you're developing locally and have some changes you want to publish, you need to push it to a shared branch on GitLab.

```
git push origin/branchname
```

When you push to a branch you'll be given a helpful link to make a merge request.

## Merging

When a feature is complete and ready to be implemented, we'll merge a development branch into master. This can be helpfully done on the GitLab repository website. To begin, go to branchname.

1. Click the blue "Create merge request" buttion, and verify that it is from branch `yourname` into `master`.
2. Write a description, and click confirm.
3. Go to the Merge Requests page, which is the button on the left bar with two mostly parralel lines.
4. View the Open requests, and click the green "merge" button. Do not delete the source afterward, as that kills the branch.
5. Note you can also Close merge requests from here, and view all other Merge Request history.

## Helpful tips

### Show Status

Using `git status` is a helpful command to show staged, added, changed, or deleted files.