var uuid = require('uuid/v1');
var rosterid, leagueid, userid, name, description, inventory, lineup, next_lineup, last_score;

exports.Roster = function(leagueid, userid, name, description) {
    this.rosterid = uuid();
    this.leagueid = leagueid;
    this.userid = userid;
    this.name = name;
    this.description = description;
    this.inventory = [];
    this.lineup = [];
    this.next_lineup = [];
    this.last_score = null;
}
