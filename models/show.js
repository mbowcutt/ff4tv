var imdbid, title, poster_url, current_rating;

exports.Show = function(imdbid, title, poster_url, current_rating) {
    this.imdbid = imdbid;
    this.title = title;
    this.poster_url = poster_url;
    this.current_rating = current_rating;
}
