var uuid = require('uuid/v1');
var leagueid, name, description, rosters, max_rosters;
var db_user = require('./../controllers/user');
var db_show = require('./../controllers/show');
var db_roster = require('./../controllers/roster');
var db_league = require('./../controllers/league');
var imdb = require('./../controllers/imdb');

exports.League = function(name, description, rosters, max_rosters){
    this.leagueid = uuid();
    this.name = name;
    this.description = description;
    this.rosters = rosters;
    this.max_rosters = max_rosters;
}

exports.score = async function(req){
    var r = req.session["league"].rosters
    for(var i = 0; i < r.length; i++){
	var roster = r[i];
	var rost = await db_roster.read(roster);
	for(var j = 0; j < rost.lineup.length; j++){
	    var activeshow = rost.lineup[j];
	    var show = await db_show.read(activeshow);
	    show.current_rating = await imdb.getrating(show.imdbid);
	    await db_show.update(show);
	}
    }
}

exports.weekend = async function(req){
    var r = req.session["league"].rosters
    for(var i = 0; i < r.length; i++){
	var roster = r[i];
	var rost = await db_roster.read(roster);
	var sum = 0;
	for(var j = 0; j < rost.lineup.length; j++){
	    var activeshow = rost.lineup[j];
	    var show = await db_show.read(activeshow);
	    show.current_rating = await imdb.getrating(show.imdbid);
	    await db_show.update(show);
	    sum = sum + show.current_rating;
	}
	rost.last_score = sum;
	rost.lineup = rost.next_lineup;
	await db_roster.update(rost);
    }
}

exports.remove = async function(req){
    // Load array of roster ids
    var r = req.session["league"].rosters;

    // find roster id to remove 
    var index = r.indexOf(req.session["roster"].rosterid);
    if (index > -1)
	r.splice(index, 1);


}

exports.delete = async function(req) {
    var rosterids = req.session["league"].rosters;
    for (var i = 0; i < rosterids.length; i++){

	// read roster and user object
	var roster = await db_roster.read(rosterids[i]);
	
	var user = await db_user.read(roster.userid);

	await db_roster.delete(rosterids[i])

	user.rosterid = null;
	user.admin = null;
	await db_user.update(user);
    }
    
    await db_league.delete(req.session["league"].leagueid);
}

exports.leagueDelete = async function(req){
	var a;
	var ros_id;
	var ros;
	var use;
	for (a = 0; a < req.session["league"].rosters.length; a++)
	{
		ros_id = req.session["league"].rosters[a];
		ros = await db_roster.read(ros_id);
		use = await db_user.read(ros.userid);
		use.rosterid = null;
		use.admin = null;
		db_user.update(use);
		db_roster.delete(ros_id);
	}
	db_league.delete(req.session["league"].leagueid);
	req.session["league"] = null;
	req.session["roster"] = null;
}

