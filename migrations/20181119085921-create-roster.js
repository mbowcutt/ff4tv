'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('rosters', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id: {
        type: Sequelize.UUIDV4
      },
      leagueID: {
        type: Sequelize.INTEGER
      },
      userID: {
        type: Sequelize.INTEGER
      },
      allShows: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      activeShows: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      nextActiveShows: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      activeHistory: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('rosters');
  }
};